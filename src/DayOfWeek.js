import format   from 'vegas-js-core/src/strings/fastformat'
import notEmpty from 'vegas-js-core/src/strings/notEmpty'

/**
 * Helper class to manipulates and formats the day of weeks expressions.
 * See the <a href="https://schema.org/DayOfWeek">https://schema.org/DayOfWeek</a> definition.
 * @name DayOfWeek
 * @memberof system.calendar
 * @class
 * @example
 * let dayOfWeek = new DayOfWeek() ;
 *
 * console.log( dayOfWeek.parse('Mo') ) ;
 * console.log( dayOfWeek.parse('Mo,Th,Ph') ) ;
 * console.log( dayOfWeek.parse('Mo-Th,Fr,Sa-Su,Ph,Sh') ) ;
 * console.log( dayOfWeek.format(['Ph','Sh'],true) ) ;
 */
class DayOfWeek
{
    /**
     * @constructor
     * @memberof system.calendar.DayOfWeek
     * @param {Object} init The generic object to initialize this instance.
     */
    constructor( init )
    {
        this.days     = [ 'Mo','Tu','We','Th','Fr','Sa','Su'] ;
        this.specials = [ 'Ph' , 'Sh' ] ;
        this.strings =
        {
            all     : 'Everyday' ,
            days    :
            [
                { id:'Mo' , name:"Monday"          , short:'Mo' } ,
                { id:'Tu' , name:"Tuesday"         , short:'Tu' } ,
                { id:'We' , name:"Wednesday"       , short:'We' } ,
                { id:'Th' , name:"Thursday"        , short:'Th' } ,
                { id:'Fr' , name:"Friday"          , short:'Fr' } ,
                { id:'Sa' , name:"Saturday"        , short:'Sa' } ,
                { id:'Su' , name:"Sunday"          , short:'Su' } ,
                { id:'Ph' , name:"Public holidays" , short:'Ph' } ,
                { id:'Sh' , name:"School holidays" , short:'Sh' }
            ],
            none     : '' ,
            specials : '{0} / {1}' ,
            week     : 'Working days' ,
            weekend  : 'Week end'
        };

        if( init )
        {
            for( let prop in init )
            {
                this[prop] = init[prop] ;
            }
        }
    }

    /**
     * Format the specific days representation.
     * @param {Array} days The days configuration to format.
     * @param {boolean} [humanReadable=false] Indicates if the result must be a human readable expression or not.
     * @memberof system.calendar.DayOfWeek
     * @function
     * @returns {string} The formatted representation of the days of week.
     */
    format = ( days , humanReadable = false ) =>
    {
        let exp = '' ;

        if( !days || !(days instanceof Array) || days.length === 0 )
        {
            return ( humanReadable === true ) ? this.strings.none : '' ;
        }

        let i ;
        let cur ;
        let pre ;
        let pos ;

        let len = days.length ;

        for( i = 0 ; i < len ; i++ )
        {
            if( pre )
            {
                exp += ',' ;
                pre = null ;
            }

            cur = days[i] ;

            if( this.days.indexOf( cur ) > -1 )
            {
                pos  = this.days.indexOf( cur ) ;
                exp += cur ;

                if( days[i+1] )
                {
                    if( this.days.indexOf(days[i+1]) === (pos+1) )
                    {
                        do
                        {
                            cur = days[i+1] ;
                            i++ ;
                            pos++ ;
                        }
                        while ( this.days.indexOf(days[i+1]) === (pos+1) );

                        exp += '-' + cur ;
                    }
                }
            }
            else if( this.specials.indexOf( cur ) > -1 )
            {
                pos = this.specials.indexOf( cur ) ;
                exp += cur ;
            }

            pre = cur ;
        }

        if( humanReadable === true )
        {
            let items ;

            switch( exp )
            {
                case '' :
                {
                    return this.strings.none ;
                }
                case 'Mo-Fr' :
                {
                    return this.strings.week ;
                }
                case 'Sa-Su' :
                {
                    return this.strings.weekend ;
                }
                case 'Mo-Su' :
                {
                    return this.strings.all ;
                }
                case 'Ph,Sh' :
                {
                    items = this.strings.days.filter( value => value && (value.id === 'Ph' || value.id === 'Sh') ) ;
                    return format( this.strings.specials, items[0].name , items[1].name );
                }
                default :
                {
                    if( this.days.indexOf(exp) > -1 )
                    {
                        items = this.strings.days.filter( value => value && value.id === exp ) ;
                        return items[0].name ;
                    }

                    len = this.days.length ;
                    for( i = 0 ; i<len ; i++ )
                    {
                        cur = this.days[i] ;
                        items = this.strings.days.filter( value => value && value.id === cur ) ;
                        if( items && items.length > 0 )
                        {
                            exp = exp.replace( cur , items[0].short ) ;
                        }
                    }

                    len = this.specials.length ;
                    for( i = 0 ; i<len ; i++ )
                    {
                        cur = this.specials[i] ;
                        items = this.strings.days.filter( value => value && value.id === cur ) ;
                        if( items && items.length > 0 )
                        {
                            exp = exp.replace( cur , items[0].name ) ;
                        }
                    }
                }
            }
        }

        return exp ;
    }

    /**
     * Parse the string expression.
     * @param {string} expression
     * @memberof system.calendar.DayOfWeek
     * @function
     * @returns {Array} The array representation of all weeks defines in the passed-in string expression.
     */
    parse = expression =>
    {
        let collector = {} ;
        let days      = [] ;

        let i, j, count, len ;

        if( notEmpty( expression ) )
        {
            let items = expression.split(',') ;

            let item ;

            len = items.length ;

            for( i = 0 ; i<len ; i++ )
            {
                item = items[i] ;

                switch( true )
                {
                    case this.days.indexOf( item ) > -1  :
                    case this.specials.indexOf( item ) > -1 :
                    {
                        collector[item] = true ;
                        break ;
                    }

                    case ( item.indexOf('-') > -1 ) :
                    {
                        item = item.split('-') ;
                        if( item.length === 2 )
                        {
                            let first = this.days.indexOf(item[0]) ;
                            let last  = this.days.indexOf(item[1]) ;
                            if( (first > -1) && (last > -1) && (last > first) )
                            {
                                let subDays = this.days.slice( first , last+1 ) ;
                                count = subDays.length ;
                                for( j = 0 ; j<count ; j++ )
                                {
                                    collector[subDays[j]] = true ;
                                }
                            }
                        }
                        break ;
                    }
                }
            }
        }

        len = this.days.length ;
        for( i = 0 ; i<len ; i++ )
        {
            if( collector[this.days[i]] === true )
            {
                days.push(this.days[i]) ;
            }
        }

        len = this.specials.length ;
        for( i = 0 ; i<len ; i++ )
        {
            if( collector[this.specials[i]] === true )
            {
                days.push(this.specials[i]) ;
            }
        }

        return days ;
    }
}

export default DayOfWeek ;