# VEGAS JS CALENDAR OpenSource library - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.2] - 2020-03-17
### Changed
* Based on vegas-js-core 1.0.9

## [1.0.0] - 2018-11-08
### Added
* First version

