const presets =
[
    [ "@babel/preset-env" , { "modules" : "auto" } ],
    "@babel/preset-react"
];

const plugins =
[
    [
        "@babel/plugin-transform-runtime" ,
        {
            corejs       : false,
            helpers      : false ,
            useESModules : true
        }
    ]
];

module.exports = { presets, plugins };