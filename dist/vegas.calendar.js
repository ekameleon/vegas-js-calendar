/**
 * A library provides a framework unified for manipulating calendar stufs. - version: 1.0.3 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.vegas_calendar = factory());
}(this, (function () { 'use strict';

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);

      if (enumerableOnly) {
        symbols = symbols.filter(function (sym) {
          return Object.getOwnPropertyDescriptor(object, sym).enumerable;
        });
      }

      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread2(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function fastformat(pattern, ...args) {
    if (pattern === null || !(pattern instanceof String || typeof pattern === 'string')) {
      return "";
    }
    if (args.length > 0) {
      args = [].concat.apply([], args);
      let len = args.length;
      for (let i = 0; i < len; i++) {
        pattern = pattern.replace(new RegExp("\\{" + i + "\\}", "g"), args[i]);
      }
    }
    return pattern;
  }

  const notEmpty = value => (typeof value === 'string' || value instanceof String) && value !== '';

  var DayOfWeek =
  function DayOfWeek(init) {
    var _this = this;
    _classCallCheck(this, DayOfWeek);
    _defineProperty(this, "format", function (days) {
      var humanReadable = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var exp = '';
      if (!days || !(days instanceof Array) || days.length === 0) {
        return humanReadable === true ? _this.strings.none : '';
      }
      var i;
      var cur;
      var pre;
      var pos;
      var len = days.length;
      for (i = 0; i < len; i++) {
        if (pre) {
          exp += ',';
          pre = null;
        }
        cur = days[i];
        if (_this.days.indexOf(cur) > -1) {
          pos = _this.days.indexOf(cur);
          exp += cur;
          if (days[i + 1]) {
            if (_this.days.indexOf(days[i + 1]) === pos + 1) {
              do {
                cur = days[i + 1];
                i++;
                pos++;
              } while (_this.days.indexOf(days[i + 1]) === pos + 1);
              exp += '-' + cur;
            }
          }
        } else if (_this.specials.indexOf(cur) > -1) {
          pos = _this.specials.indexOf(cur);
          exp += cur;
        }
        pre = cur;
      }
      if (humanReadable === true) {
        var items;
        switch (exp) {
          case '':
            {
              return _this.strings.none;
            }
          case 'Mo-Fr':
            {
              return _this.strings.week;
            }
          case 'Sa-Su':
            {
              return _this.strings.weekend;
            }
          case 'Mo-Su':
            {
              return _this.strings.all;
            }
          case 'Ph,Sh':
            {
              items = _this.strings.days.filter(function (value) {
                return value && (value.id === 'Ph' || value.id === 'Sh');
              });
              return fastformat(_this.strings.specials, items[0].name, items[1].name);
            }
          default:
            {
              if (_this.days.indexOf(exp) > -1) {
                items = _this.strings.days.filter(function (value) {
                  return value && value.id === exp;
                });
                return items[0].name;
              }
              len = _this.days.length;
              for (i = 0; i < len; i++) {
                cur = _this.days[i];
                items = _this.strings.days.filter(function (value) {
                  return value && value.id === cur;
                });
                if (items && items.length > 0) {
                  exp = exp.replace(cur, items[0]["short"]);
                }
              }
              len = _this.specials.length;
              for (i = 0; i < len; i++) {
                cur = _this.specials[i];
                items = _this.strings.days.filter(function (value) {
                  return value && value.id === cur;
                });
                if (items && items.length > 0) {
                  exp = exp.replace(cur, items[0].name);
                }
              }
            }
        }
      }
      return exp;
    });
    _defineProperty(this, "parse", function (expression) {
      var collector = {};
      var days = [];
      var i, j, count, len;
      if (notEmpty(expression)) {
        var items = expression.split(',');
        var item;
        len = items.length;
        for (i = 0; i < len; i++) {
          item = items[i];
          switch (true) {
            case _this.days.indexOf(item) > -1:
            case _this.specials.indexOf(item) > -1:
              {
                collector[item] = true;
                break;
              }
            case item.indexOf('-') > -1:
              {
                item = item.split('-');
                if (item.length === 2) {
                  var first = _this.days.indexOf(item[0]);
                  var last = _this.days.indexOf(item[1]);
                  if (first > -1 && last > -1 && last > first) {
                    var subDays = _this.days.slice(first, last + 1);
                    count = subDays.length;
                    for (j = 0; j < count; j++) {
                      collector[subDays[j]] = true;
                    }
                  }
                }
                break;
              }
          }
        }
      }
      len = _this.days.length;
      for (i = 0; i < len; i++) {
        if (collector[_this.days[i]] === true) {
          days.push(_this.days[i]);
        }
      }
      len = _this.specials.length;
      for (i = 0; i < len; i++) {
        if (collector[_this.specials[i]] === true) {
          days.push(_this.specials[i]);
        }
      }
      return days;
    });
    this.days = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];
    this.specials = ['Ph', 'Sh'];
    this.strings = {
      all: 'Everyday',
      days: [{
        id: 'Mo',
        name: "Monday",
        "short": 'Mo'
      }, {
        id: 'Tu',
        name: "Tuesday",
        "short": 'Tu'
      }, {
        id: 'We',
        name: "Wednesday",
        "short": 'We'
      }, {
        id: 'Th',
        name: "Thursday",
        "short": 'Th'
      }, {
        id: 'Fr',
        name: "Friday",
        "short": 'Fr'
      }, {
        id: 'Sa',
        name: "Saturday",
        "short": 'Sa'
      }, {
        id: 'Su',
        name: "Sunday",
        "short": 'Su'
      }, {
        id: 'Ph',
        name: "Public holidays",
        "short": 'Ph'
      }, {
        id: 'Sh',
        name: "School holidays",
        "short": 'Sh'
      }],
      none: '',
      specials: '{0} / {1}',
      week: 'Working days',
      weekend: 'Week end'
    };
    if (init) {
      for (var prop in init) {
        this[prop] = init[prop];
      }
    }
  }
  ;

  /**
   * The {@link system.numeric} library contains classes and tools that provides extra <code>numeric</code> methods and implementations.
   * @summary The {@link system.numeric} library contains classes and tools that provides extra <code>numeric</code> methods and implementations.
   * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
   * @author Marc Alcaraz <ekameleon@gmail.com>
   * @namespace system.calendar
   * @memberof system
   */
  var data = {
    DayOfWeek: DayOfWeek
  };

  var skip = false;
  function sayHello(name = '', version = '', link = '') {
    if (skip) {
      return;
    }
    try {
      if (navigator && navigator.userAgent && navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
        const args = [`\n %c %c %c ${name} ${version} %c %c ${link} %c %c\n\n`, 'background: #ff0000; padding:5px 0;', 'background: #AA0000; padding:5px 0;', 'color: #F7FF3C; background: #000000; padding:5px 0;', 'background: #AA0000; padding:5px 0;', 'color: #F7FF3C; background: #ff0000; padding:5px 0;', 'background: #AA0000; padding:5px 0;', 'background: #ff0000; padding:5px 0;'];
        window.console.log.apply(console, args);
      } else if (window.console) {
        window.console.log(`${name} ${version} - ${link}`);
      }
    } catch (error) {
    }
  }
  function skipHello() {
    skip = true;
  }

  var metas = Object.defineProperties({}, {
    name: {
      enumerable: true,
      value: 'vegas-js-calendar'
    },
    description: {
      enumerable: true,
      value: 'A library provides a framework unified for manipulating calendar stufs.'
    },
    version: {
      enumerable: true,
      value: '1.0.3'
    },
    license: {
      enumerable: true,
      value: "MPL-2.0 OR GPL-2.0+ OR LGPL-2.1+"
    },
    url: {
      enumerable: true,
      value: 'https://bitbucket.org/ekameleon/vegas-js-calendar'
    }
  });
  var bundle = _objectSpread2({
    metas: metas,
    sayHello: sayHello,
    skipHello: skipHello
  }, data);
  try {
    if (window) {
      window.addEventListener('load', function load() {
        window.removeEventListener("load", load, false);
        sayHello(metas.name, metas.version, metas.url);
      }, false);
    }
  } catch (ignored) {}

  return bundle;

})));
//# sourceMappingURL=vegas.calendar.js.map
