"use strict" ;

import DayOfWeek from '../../src/DayOfWeek'

import chai  from 'chai' ;
const assert = chai.assert ;

describe( 'system.calendar.DayOfWeek' , () =>
{
    it('DayOfWeek not null', () =>
    {
        assert.isNotNull( DayOfWeek ) ;
    });
});
